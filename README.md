# Frontend E-Commersse

# Getting started

## Install Node

Install [Node.JS LTS version](https://nodejs.org/en/download/)

## To get the React App running locally

- Clone this repo
- `cd /path/where/your/cloned/the/repo`
- `npm install` to install all required dependencies
- Setup `.env` Variable

```PORT=7000
REACT_APP_API_URL=your_server_url
REACT_APP_API_KEY=your_server_api_key
```

- Start React App `npm start`

## Dependencies

- [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces
- [MobX](https://mobx.js.org/README.html) - State Management
- [axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
- [Tailwind CSS](https://tailwindcss.com/docs/installation) - CSS framework

# Author

### [Indra Pradipta](https://github.com/iprdpta)
