import { useState } from 'react';

export const useForm = (initialValues) => {
  const [values, setValues] = useState(initialValues);

  return [
    values,
    (key, newValue) => {
      setValues({
        ...values,
        [key]: newValue,
      });
    },
    (newValues) => {
      setValues({
        ...values,
        ...newValues,
      });
    },
  ];
};
