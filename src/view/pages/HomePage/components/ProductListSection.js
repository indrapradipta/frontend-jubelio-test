import React, { useCallback, useRef, useState } from 'react';
import ProductCard from '../../../components/ProductCard';
import ProductDetailModal from '../../../components/ProductDetailModal';

const ProductListSection = ({ isLoading, productData, setPage }) => {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [productId, setProductId] = useState(null);
  const observer = useRef();
  const lastBookElementRef = useCallback(
    (node) => {
      if (isLoading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          setPage();
        }
      });
      if (node) observer.current.observe(node);
    },
    [isLoading, setPage]
  );

  const handleProductDetailModal = (id) => {
    setProductId(id);
    setIsOpenModal(!isOpenModal);
  };

  return (
    <div className="grid lg:grid-cols-4 grid-cols-2 gap-4">
      {productData.map((item, index) => {
        if (productData.length === index + 1) {
          return (
            <div
              onClick={() => handleProductDetailModal(item.id)}
              ref={lastBookElementRef}
              key={index}
            >
              <ProductCard data={item} />
            </div>
          );
        } else {
          return (
            <div onClick={() => handleProductDetailModal(item.id)} key={index}>
              <ProductCard data={item} />
            </div>
          );
        }
      })}
      {isLoading && <div>Loading...</div>}
      <ProductDetailModal
        isOpen={isOpenModal}
        setIsOpen={setIsOpenModal}
        productId={productId}
      />
    </div>
  );
};

export default ProductListSection;
