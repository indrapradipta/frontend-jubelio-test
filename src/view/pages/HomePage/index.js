import React, { useCallback, useEffect } from 'react';
import { inject, observer } from 'mobx-react';
import { GoPackage } from 'react-icons/go';
import Navbar from '../../components/Navbar';
import ProductListSection from './components/ProductListSection';

const HomePage = observer(({ productStore }) => {
  const { isLoading, productData, fetchProducts, setPage } = productStore;

  const fetchProductData = useCallback(async () => {
    await fetchProducts();
  }, [fetchProducts]);

  useEffect(() => {
    fetchProductData();
  }, [fetchProductData]);

  return (
    <div className="overflow-x-hidden">
      <Navbar />
      <div className="bg-gray-100 lg:p-20 p-4 space-y-8 h-screen w-screen">
        <div className="flex flex-row justify-between items-center">
          <div>
            <label className="font-bold text-xl">Product List</label>
          </div>
        </div>
        {isLoading ? (
          <div>Loading...</div>
        ) : !isLoading && productData.length > 0 ? (
          <ProductListSection
            productData={productData}
            isLoading={isLoading}
            setPage={setPage}
          />
        ) : (
          <div className="w-full h-full flex flex-col justify-center items-center">
            <GoPackage size={100} />
            <div className="font-bold text-xl">Products is Empty</div>
            <div>Please Add or Import Product</div>
          </div>
        )}
      </div>
    </div>
  );
});

export default inject('productStore')(HomePage);
