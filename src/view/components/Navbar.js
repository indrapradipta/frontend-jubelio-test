import React, { useState } from 'react';
import AddProductModal from './AddProductModal';
import ImportProductModal from './ImportProductModal';

function Navbar() {
  const [isOpenModalAdd, setIsOpenModalAdd] = useState(false);
  const [isOpenModalImport, setIsOpenModalImport] = useState(false);

  return (
    <div className="fixed z-10 w-screen shadow-lg h-16 bg-white flex flex-row justify-between pl-10 pr-10 items-center">
      <div>
        <label className="font-bold md:text-lg text-xs">
          Simple E-Commerse
        </label>
      </div>
      <div className="space-x-4 flex flex-row">
        <div
          onClick={() => setIsOpenModalImport(!isOpenModalImport)}
          className="cursor-pointer md:text-lg text-xs hover:text-blue-400 font-bold border-2 md:p-2 rounded-lg hover:border-blue-400 border-black flex flex-row justify-center items-center"
        >
          Import Product
        </div>
        <div
          onClick={() => setIsOpenModalAdd(!isOpenModalAdd)}
          className="cursor-pointer md:text-lg text-xs hover:text-blue-400 font-bold border-2 p-2 rounded-lg hover:border-blue-400 border-black flex flex-row justify-center items-center"
        >
          Add Product
        </div>
      </div>

      <ImportProductModal
        isOpen={isOpenModalImport}
        setIsOpen={setIsOpenModalImport}
      />
      <AddProductModal isOpen={isOpenModalAdd} setIsOpen={setIsOpenModalAdd} />
    </div>
  );
}

export default Navbar;
