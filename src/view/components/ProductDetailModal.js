import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { AiFillCloseCircle } from 'react-icons/ai';

import productApi from '../../api/product-api';
import { formatPrice } from '../../utils/formatPrice';
import Button from './Button';
import EditProductModal from './EditProductModal';
import DeleteProductModal from './DeleteProductModal';

const ProductDetailModal = ({ isOpen, setIsOpen, productId }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isOpenModalEdit, setIsOpenModalEdit] = useState(false);
  const [isOpenModalDelete, setIsOpenModalDelete] = useState(false);
  const [data, setData] = useState(null);

  const fetchProductDetail = async (id) => {
    setIsLoading(true);
    try {
      const res = await productApi.productDetail(id);
      setData(res.data.result);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (productId) fetchProductDetail(productId);
  }, [productId, isOpen, isOpenModalEdit]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        contentLabel="Example Modal"
        ariaHideApp={false}
        onRequestClose={() => setIsOpen(false)}
        style={{
          content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            overflowY: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            maxHeight: '100vh',
          },
          overlay: {
            zIndex: 100,
          },
        }}
      >
        <div className="md:w-96 w-full h-full space-y-10">
          <div
            className="cursor-pointer w-full h-full flex flex-row justify-between"
            onClick={() => {
              setIsOpen(false);
            }}
          >
            <label className="font-bold text-xl">Product Detail</label>
            <AiFillCloseCircle className="text-red-500" size={24} />
          </div>
          {!isLoading && data ? (
            <div className="flex flex-col space-y-4">
              <div className="w-full h-full flex justify-center items-center">
                <img
                  src={data?.image}
                  className="w-96 h-96 bg-cover"
                  alt="Product"
                />
              </div>
              <div className="flex flex-row justify-between items-center space-x-4 mt-4 z-20x">
                <Button
                  type="normal"
                  onClick={() => setIsOpenModalEdit(!isOpenModalEdit)}
                >
                  Edit
                </Button>
                <Button
                  onClick={() => setIsOpenModalDelete(!isOpenModalDelete)}
                  type="danger"
                >
                  Delete
                </Button>
              </div>
              <div className="text-sm">SKU : {data.sku}</div>
              <div className="text-xl">{data.name}</div>
              <div className="text-xl font-bold">
                Rp{formatPrice(data.price)}
              </div>
              <div className="space-y-2">
                <div>Product Description :</div>
                <div dangerouslySetInnerHTML={{ __html: data.description }} />
              </div>
            </div>
          ) : (
            <div>Loading...</div>
          )}
        </div>
      </Modal>

      <EditProductModal
        isOpen={isOpenModalEdit}
        setIsOpen={setIsOpenModalEdit}
        data={data}
        productId={productId}
      />

      <DeleteProductModal
        isOpen={isOpenModalDelete}
        setIsOpen={setIsOpenModalDelete}
        data={data}
        productId={productId}
        detailModal={setIsOpen}
      />
    </>
  );
};

export default ProductDetailModal;
