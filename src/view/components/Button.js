import React from 'react';
import { AiOutlineLoading } from 'react-icons/ai';

const Button = ({ isLoading, onClick, children, type = 'normal' }) => {
  const buttonType = {
    normal: 'bg-blue-500 hover:bg-blue-700',
    danger: 'bg-red-500 hover:bg-red-700',
  };
  return (
    <button
      onClick={onClick}
      className={`w-full text-white font-bold py-2 px-4 rounded flex justify-center items-center ${buttonType[type]}`}
    >
      {isLoading ? (
        <AiOutlineLoading className="animate-spin text-2xl" />
      ) : (
        children
      )}
    </button>
  );
};

export default Button;
