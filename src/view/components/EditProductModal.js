import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { AiFillCloseCircle } from 'react-icons/ai';

import { useForm } from '../../helpers/useForm';
import productApi from '../../api/product-api';
import Button from '../components/Button';
import { toast } from 'react-toastify';
import { inject, observer } from 'mobx-react';
import { formatError } from '../../utils/formsErrorsFormater';

const EditProductModal = ({
  isOpen,
  setIsOpen,
  productId,
  data,
  productStore,
}) => {
  const initStateForm = {
    name: '',
    sku: '',
    image: '',
    price: '',
    description: '',
  };
  const [form, setForm, setAllForm] = useForm({ ...initStateForm });
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState(null);

  useEffect(() => {
    if (data)
      setAllForm({
        name: data.name,
        sku: data.sku,
        image: data.image,
        price: data.price,
        description: data.description,
      });
    setErrors(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, isOpen]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const res = await productApi.updateProduct(productId, form);
      toast.success(res.data.message);
      await productStore.fetchProducts(1);
      setAllForm({ ...initStateForm });
      setErrors(null);
      setIsOpen(false);
      setIsLoading(false);
    } catch (e) {
      toast.error(e.data.error);
      setErrors(formatError(e.data.validation.keys, e.data.message));
      setIsLoading(false);
    }
  };

  const forms = [
    {
      name: 'name',
      label: 'Product Name',
    },
    {
      name: 'sku',
      label: 'SKU',
    },
    {
      name: 'image',
      label: 'Image URL',
    },
    {
      name: 'price',
      label: 'Price',
      input: 'number',
    },
    {
      name: 'description',
      label: 'Description',
      type: 'textarea',
    },
  ];

  return (
    <Modal
      isOpen={isOpen}
      contentLabel="Example Modal"
      ariaHideApp={false}
      style={{
        content: {
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          overflowY: 'auto',
          marginRight: '-50%',
          transform: 'translate(-50%, -50%)',
          maxHeight: '100vh',
        },
        overlay: {
          zIndex: 100,
        },
      }}
    >
      <div className="md:w-96 w-full h-full space-y-10">
        <div
          className="w-full h-full flex flex-row justify-between cursor-pointer"
          onClick={() => setIsOpen(false)}
        >
          <label className="font-bold text-xl">Edit Product</label>
          <AiFillCloseCircle className="text-red-500" size={24} />
        </div>
        <form onSubmit={handleSubmit} className="flex flex-col space-y-4">
          {forms.map((item, index) => (
            <div key={index} className="flex flex-col space-y-2">
              <label className="font-bold">{item.label}</label>
              {item.type === 'textarea' ? (
                <textarea
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={form[item.name]}
                  name={item.name}
                  onChange={(e) => setForm(item.name, e.target.value)}
                />
              ) : (
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={form[item.name]}
                  name={item.name}
                  type={item.input}
                  onChange={(e) => setForm(item.name, e.target.value)}
                />
              )}
              {errors && errors[item.name] && (
                <div className="text-red-500">{errors[item.name]}</div>
              )}
            </div>
          ))}
          <Button onClick={handleSubmit} isLoading={isLoading}>
            SAVE
          </Button>
        </form>
      </div>
    </Modal>
  );
};

export default inject('productStore')(observer(EditProductModal));
