import React, { useState } from 'react';
import Modal from 'react-modal';
import { AiFillCloseCircle } from 'react-icons/ai';

import productApi from '../../api/product-api';
import Button from '../components/Button';
import { toast } from 'react-toastify';
import { inject, observer } from 'mobx-react';

const toastOpt = {
  position: 'top-right',
  autoClose: 5000,
  hideProgressBar: false,
  newestOnTop: false,
  closeOnClick: true,
  rtl: false,
  pauseOnFocusLoss: true,
  draggable: true,
  pauseOnHover: true,
};
const ImportProduct = ({ isOpen, setIsOpen, productStore }) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const id = toast.loading('Importing Products');
    try {
      setIsOpen(false);
      setIsLoading(false);
      const res = await productApi.importProducts();
      toast.update(id, {
        render: res.data.message,
        type: 'success',
        isLoading: false,
        ...toastOpt,
      });
      await productStore.fetchProducts(1);
    } catch (e) {
      toast.update(id, {
        render: e.data.error,
        type: 'error',
        isLoading: false,
        ...toastOpt,
      });
      setIsLoading(false);
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      contentLabel="Example Modal"
      ariaHideApp={false}
      style={{
        content: {
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          overflowY: 'auto',
          marginRight: '-50%',
          transform: 'translate(-50%, -50%)',
          maxHeight: '100vh',
        },
        overlay: {
          zIndex: 100,
        },
      }}
    >
      <div className="md:w-96 w-full h-full space-y-10">
        <div
          className="w-full h-full flex flex-row justify-between cursor-pointer"
          onClick={() => setIsOpen(false)}
        >
          <label className="font-bold text-xl">Import Product</label>
          <AiFillCloseCircle className="text-red-500" size={24} />
        </div>
        <div className="flex flex-col space-y-4">
          <div>Import Product from Elevenia ?</div>
          <div className="flex flex-row space-x-4">
            <Button type="danger" onClick={handleSubmit} isLoading={isLoading}>
              YES
            </Button>
            <Button
              type="normal"
              onClick={() => setIsOpen(false)}
              isLoading={isLoading}
            >
              NO
            </Button>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default inject('productStore')(observer(ImportProduct));
