import React, { useState } from 'react';
import Modal from 'react-modal';
import { AiFillCloseCircle } from 'react-icons/ai';

import productApi from '../../api/product-api';
import Button from '../components/Button';
import { toast } from 'react-toastify';
import { inject, observer } from 'mobx-react';

const DeleteProductModal = ({
  isOpen,
  setIsOpen,
  productId,
  data,
  detailModal,
  productStore,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const res = await productApi.deleteProduct(productId);
      toast.success(res.data.message);
      setIsOpen(false);
      detailModal(false);
      setIsLoading(false);
      await productStore.fetchProducts(1);
    } catch (e) {
      toast.error(e.data.error);
      setIsLoading(false);
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      contentLabel="Example Modal"
      ariaHideApp={false}
      style={{
        content: {
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          overflowY: 'auto',
          marginRight: '-50%',
          transform: 'translate(-50%, -50%)',
          maxHeight: '100vh',
        },
        overlay: {
          zIndex: 100,
        },
      }}
    >
      <div className="md:w-96 w-full h-full space-y-10">
        <div
          className="w-full h-full flex flex-row justify-between cursor-pointer"
          onClick={() => setIsOpen(false)}
        >
          <label className="font-bold text-xl">Delete Product</label>
          <AiFillCloseCircle className="text-red-500" size={24} />
        </div>
        <div className="flex flex-col space-y-4">
          <div>Delete {data?.name} product?</div>
          <div className="flex flex-row space-x-4">
            <Button type="danger" onClick={handleSubmit} isLoading={isLoading}>
              YES
            </Button>
            <Button
              type="normal"
              onClick={() => setIsOpen(false)}
              isLoading={isLoading}
            >
              NO
            </Button>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default inject('productStore')(observer(DeleteProductModal));
