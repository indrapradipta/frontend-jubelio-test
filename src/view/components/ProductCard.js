import React from 'react';
import { formatPrice } from '../../utils/formatPrice';

function ProductCard({ data }) {
  const { image, name, price, sku } = data;

  return (
    <div className="relative bg-white shadow-md rounded-lg flex flex-col cursor-pointer hover:scale-110 hover:z-10 transition-all duration-100">
      <div className="bg-blue-300 w-full h-56 rounded-t-lg border-b-2">
        <img src={image} className="w-full h-full bg-cover" alt="Product" />
      </div>
      <div className="p-2 flex flex-col">
        <div>
          <label className="text-sm truncate">SKU : {sku}</label>
        </div>
        <div className="truncate">
          <label className="text-lg">{name}</label>
        </div>
        <div>
          <label className="font-bold text-lg truncate">
            Rp{formatPrice(price)}
          </label>
        </div>
      </div>
    </div>
  );
}

export default ProductCard;
