export const formatPrice = (numbers) => {
  return new Intl.NumberFormat(['ban', 'id']).format(numbers);
};
