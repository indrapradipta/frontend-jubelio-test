export const formatError = (x, m) => {
  const sm = m.split('.');
  const err = {};
  for (let i = 0; i < x.length; i++) {
    err[x[i]] = sm.find((xs) => xs.includes(x[i]));
  }

  return err;
};
