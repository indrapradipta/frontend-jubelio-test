import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './view/pages/HomePage';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="*" exact={true} element={<div>Not Found</div>} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
