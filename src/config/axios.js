import axios from 'axios';

const apiAdapter = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 100000,
});

// Add a request interceptor
apiAdapter.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
apiAdapter.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    // console.log(error.response);
    return Promise.reject(error.response);
  }
);

export const setAuthorization = (token) => {
  apiAdapter.defaults.headers.common.Authorization = `Bearer ${token}`;
  // const abc = (apiAdapter.defaults.headers.common.Authorization = `Bearer ${token}`);
  // // console.log('ini token', abc);
};

export const removeAuthorization = () => {
  apiAdapter.defaults.headers.common.Authorization = null;
};

export default apiAdapter;
