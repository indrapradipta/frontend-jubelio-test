import { action, makeObservable, observable, runInAction } from 'mobx';
import { toast } from 'react-toastify';
import productApi from '../api/product-api';

export class ProductStore {
  productData = [];
  page = 1;
  limit = 8;
  pages = 1;
  isLoading = true;

  constructor() {
    makeObservable(this, {
      productData: observable,
      fetchProducts: action,
      page: observable,
      pages: observable,
      isLoading: observable,
    });
  }

  fetchProducts = async (page) => {
    try {
      const res = await productApi.productsList({
        page: page || this.page,
        limit: this.limit,
      });
      const data = res.data.result;
      if (res) {
        runInAction(() => {
          this.isLoading = false;
          this.page = data.page;
          this.pages = data.pages;
          this.limit = data.limit;
          if (page === 1) {
            this.productData = data.data;
          } else {
            this.productData = [...this.productData, ...data.data];
          }
        });
      }
    } catch (e) {
      toast.error(e?.data.error);
      runInAction(() => {
        this.isLoading = false;
      });
    }
  };

  setPage = async () => {
    if (this.page < this.pages) {
      runInAction(() => {
        this.page = this.page + 1;
      });
      await this.fetchProducts(this.page);
    }
  };
}
