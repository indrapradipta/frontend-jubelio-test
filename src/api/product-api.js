import axios, { setAuthorization } from '../config/axios';
import queryString from 'query-string';

const productApi = {
  productsList: async (query) => {
    setAuthorization(process.env.REACT_APP_API_KEY);
    return await axios.get('/products?' + queryString.stringify(query));
  },
  importProducts: async () => {
    setAuthorization(process.env.REACT_APP_API_KEY);
    return await axios.get('/import-products');
  },
  productDetail: async (id) => {
    setAuthorization(process.env.REACT_APP_API_KEY);
    return await axios.get(`/product/${id}`);
  },
  createProduct: async (body) => {
    setAuthorization(process.env.REACT_APP_API_KEY);
    return await axios.post('/product', body);
  },
  updateProduct: async (id, body) => {
    setAuthorization(process.env.REACT_APP_API_KEY);
    return await axios.put(`/product/${id}`, body);
  },
  deleteProduct: async (id, body) => {
    setAuthorization(process.env.REACT_APP_API_KEY);
    return await axios.delete(`/product/${id}`, body);
  },
};

export default productApi;
